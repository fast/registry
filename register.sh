#!/usr/bin/bash

set -e

if [ $# -ne 1 ]; then
    echo "usage: bash $0 <package-dir>"
    exit 1
fi
packagedir=$1
cdir=$(pwd)

# Ensure that the registry is properly added to Julia
julia -e 'using Pkg; Pkg.Registry.add(RegistrySpec(url = "https://gitlab.lisn.upsaclay.fr/fast/registry"))'

# Ensure that "LocalRegistry" package is installed.
julia -e 'using Pkg; Pkg.add("LocalRegistry")'

pushd $packagedir

# 1. Make sure that the package can be precompiled.
julia --project=./ -e 'using Pkg; Pkg.resolve(); Pkg.precompile()'

# 2. Run the tests if they are provided.
if [ -f test/runtests.jl ]; then
    julia --project=./ -e 'using Pkg; Pkg.test()'
else
    echo "[warning] no tests provided"
fi

# 3. Create a tag for the given version (no version check is performed).
version=$(cat Project.toml | grep version | cut -f2 -d'=' | sed -e 's/^[ \t]*//' | sed -e 's/"//g')
git tag -s v$version -m"version $version"
git push origin v$version

# 5. Register the package.
julia --project=$cdir -e 'using Pkg; Pkg.Registry.update(); using LocalRegistry; register(; registry = "FAST")'

popd
