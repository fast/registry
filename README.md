# FAST Official Registry

Registry providing the different components of [FAST](https://hebergement.universite-paris-saclay.fr/fast-asr/).

## Installation

To add this registry to your Julia installation type `]` to enter the
package mode of the REPL and then type:

```
pkg> registry add "https://gitlab.lisn.upsaclay.fr/fast/registry"
```

